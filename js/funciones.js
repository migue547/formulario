
// Evento que se ejecuta al pulsar el botón agregar
document.querySelector("input[type=submit]").addEventListener("click",function(e){
    // cancelamos el evento
    e.preventDefault();
 
    const nombre=document.querySelector("input[name=nombre]");
    const apellidos=document.querySelector("input[name=apellidos]");
    const correoElectronico=document.querySelector("input[name=correoElectronico]");
    const telefono=document.querySelector("input[name=telefono]");
    const message=document.querySelector("textarea[name=message]");
 
     
    agregarFila(nombre.value, apellidos.value, correoElectronico.value, telefono.value, message.value);
 
    nombre.value="";
    apellidos.value="";
    correoElectronico.value="";
    telefono.value="";
    message.value="";
    nombre.focus();

});

function agregarFila(nombre, apellidos, correoElectronico, telefono, message) {
    // añadimos el alumno a la tabla crando el tr, td's y el botón para eliminarlo
    const tr=document.createElement("tr");
 
    const tdNombre=document.createElement("td");
    let txt=document.createTextNode(nombre);
    tdNombre.appendChild(txt);
    tdNombre.className="nombre";
 
    const tdApellidos=document.createElement("td");
    txt=document.createTextNode(apellidos);
    tdApellidos.appendChild(txt);
    tdApellidos.className="right";

    const tdcorreoElectronico=document.createElement("td");
    txt=document.createTextNode(correoElectronico);
    tdcorreoElectronico.appendChild(txt);
    tdcorreoElectronico.className="right";

    const tdtelefono=document.createElement("td");
    txt=document.createTextNode(telefono);
    tdtelefono.appendChild(txt);
    tdtelefono.className="right";

    const tdmessage=document.createElement("td");
    txt=document.createTextNode(message);
    tdmessage.appendChild(txt);
    tdmessage.className="right";
 
 
    tr.appendChild(tdNombre);
    tr.appendChild(tdApellidos);
    tr.appendChild(tdcorreoElectronico);
    tr.appendChild(tdtelefono);
    tr.appendChild(tdmessage);

 
    const tbody=document.getElementById("listado").querySelector("tbody").appendChild(tr);
 
}
 
function InsertarFila(e) {
    const tr=this.closest("tr")
    const nombre=tr.querySelector(".nombre").innerText;
    const apellidos=tr.querySelector(".right").innerText;
    const correoElectronico=tr.querySelector(".right").innerText;
    const telefono=tr.querySelector(".right").innerText;
    const message=tr.querySelector(".right").innerText;
 
}